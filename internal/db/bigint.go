package db

import (
	"database/sql/driver"
	"fmt"
	"math/big"
)

func AsScannableBigInt(i *big.Int) *BigInt {
	return (*BigInt)(i)
}

type BigInt big.Int

func (b *BigInt) Value() (driver.Value, error) {
	if b != nil {
		return (*big.Int)(b).String(), nil
	}
	return nil, nil
}

func (b *BigInt) Scan(value interface{}) error {
	if value == nil {
		b = nil
	}

	switch t := value.(type) {
	case []uint8:
		(*big.Int)(b).SetBytes(value.([]uint8))
	case string:
		_, ok := (*big.Int)(b).SetString(value.(string), 10)
		if !ok {
			return fmt.Errorf("failed to load value to string: %v", value)
		}
	default:
		return fmt.Errorf("could not scan type %T into big.Int", t)
	}

	return nil
}
