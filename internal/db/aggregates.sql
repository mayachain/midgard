-- version 1

DROP SCHEMA IF EXISTS midgard_agg CASCADE;
CREATE SCHEMA midgard_agg;

CREATE VIEW midgard_agg.pending_adds AS
SELECT *
FROM pending_liquidity_events AS p
WHERE pending_type = 'add'
    AND NOT EXISTS(
        -- Filter out pending liquidity which was already added
        SELECT *
        FROM stake_events AS s
        WHERE
            p.cacao_addr = s.cacao_addr
            AND p.pool = s.pool
            AND p.block_timestamp <= s.block_timestamp)
    AND NOT EXISTS(
        -- Filter out pending liquidity which was withdrawn without adding
        SELECT *
        FROM pending_liquidity_events AS pw
        WHERE
            pw.pending_type = 'withdraw'
            AND p.cacao_addr = pw.cacao_addr
            AND p.pool = pw.pool
            AND p.block_timestamp <= pw.block_timestamp);

CREATE TABLE midgard_agg.watermarks (
    materialized_table varchar PRIMARY KEY,
    watermark bigint NOT NULL
);

CREATE FUNCTION midgard_agg.watermark(t varchar) RETURNS bigint
LANGUAGE SQL STABLE AS $$
    SELECT watermark FROM midgard_agg.watermarks
    WHERE materialized_table = t;
$$;

CREATE PROCEDURE midgard_agg.refresh_watermarked_view(t varchar, w_new bigint)
LANGUAGE plpgsql AS $BODY$
DECLARE
    w_old bigint;
BEGIN
    SELECT watermark FROM midgard_agg.watermarks WHERE materialized_table = t
        FOR UPDATE INTO w_old;
    IF w_new <= w_old THEN
        RAISE WARNING 'Updating % into past: % -> %', t, w_old, w_new;
        RETURN;
    END IF;
    EXECUTE format($$
        INSERT INTO midgard_agg.%1$I_materialized
        SELECT * from midgard_agg.%1$I
            WHERE $1 <= block_timestamp AND block_timestamp < $2
    $$, t) USING w_old, w_new;
    UPDATE midgard_agg.watermarks SET watermark = w_new WHERE materialized_table = t;
END
$BODY$;

-------------------------------------------------------------------------------
-- Mayaname
-------------------------------------------------------------------------------

-- TODO(muninn): replace with indexing time materialized table, a full select is 100ms.
CREATE VIEW midgard_agg.mayaname_owner_expiration AS
    SELECT DISTINCT ON (name)
        name,
        owner,
        expire
    FROM mayaname_change_events
    ORDER BY name, block_timestamp DESC;

CREATE VIEW midgard_agg.mayaname_preferred_asset AS
    SELECT DISTINCT ON (name)
        name,
        preferred_asset
    FROM mayaname_change_events
    WHERE preferred_asset IS NOT NULL
    ORDER BY name, block_timestamp DESC;

CREATE VIEW midgard_agg.mayaname_registration_height AS
    SELECT DISTINCT ON (owner, name)
        name,
        owner,
        height
    FROM mayaname_change_events
    ORDER BY owner, name, block_timestamp ASC, event_id ASC;

CREATE VIEW midgard_agg.mayaname_last_affiliate_bps AS
    SELECT DISTINCT ON (name)
        name,
        affiliate_bps
    FROM mayaname_change_events
    WHERE affiliate_bps > 0
    ORDER BY name, block_timestamp DESC, event_id DESC;

CREATE VIEW midgard_agg.mayaname_current_state AS
    SELECT DISTINCT ON (name, chain)
        change_events.name,
        change_events.chain,
        change_events.address,
        owner_expiration.owner,
        registration_height.height,
        owner_expiration.expire,
        COALESCE(owner_preferred_asset.preferred_asset, '') AS preferred_asset,
        COALESCE(last_affiliate_bps.affiliate_bps, 0) AS affiliate_bps
    FROM mayaname_change_events AS change_events
    LEFT JOIN midgard_agg.mayaname_owner_expiration AS owner_expiration
        ON owner_expiration.name = change_events.name
    LEFT JOIN midgard_agg.mayaname_preferred_asset AS owner_preferred_asset
        ON owner_preferred_asset.name = change_events.name
    LEFT JOIN midgard_agg.mayaname_registration_height AS registration_height
        ON registration_height.name = change_events.name
        AND registration_height.owner = change_events.owner
    LEFT JOIN midgard_agg.mayaname_last_affiliate_bps AS last_affiliate_bps
        ON last_affiliate_bps.name = change_events.name
    ORDER BY name, chain, block_timestamp DESC;

CREATE VIEW midgard_agg.mayaname_sub_affiliates AS
    SELECT *
    FROM (
        SELECT DISTINCT ON (change_events.name, sa.name)
            change_events.name AS name,
            sa.name AS sub_name,
            sa.affiliate_bps AS affiliate_bps
        FROM mayaname_change_events AS change_events,
            jsonb_to_recordset(change_events.sub_affiliates) AS sa(name TEXT, affiliate_bps INT)
        ORDER BY change_events.name, sa.name, block_timestamp DESC
    ) AS sub_affiliates
    WHERE affiliate_bps > 0;


-------------------------------------------------------------------------------
-- Actions
-------------------------------------------------------------------------------

--
-- Main table and its indices
--

CREATE TABLE midgard_agg.actions (
    event_id            bigint NOT NULL,
    block_timestamp     bigint NOT NULL,
    action_type         text NOT NULL,
    main_ref            text,
    addresses           text[] NOT NULL,
    transactions        text[] NOT NULL,
    assets              text[] NOT NULL,
    pools               text[],
    ins                 jsonb NOT NULL,
    outs                jsonb NOT NULL,
    fees                jsonb NOT NULL,
    meta                jsonb,
    streaming_meta      jsonb
);
-- TODO(huginn): should it be a hypertable? Measure both ways and decide!

CREATE UNIQUE INDEX swap_idx ON midgard_agg.actions (main_ref) WHERE action_type IN ('swap');

CREATE INDEX ON midgard_agg.actions (event_id DESC);
CREATE INDEX ON midgard_agg.actions (action_type, event_id DESC);
CREATE INDEX ON midgard_agg.actions (main_ref, event_id DESC);
CREATE INDEX ON midgard_agg.actions (block_timestamp, event_id DESC);

CREATE INDEX ON midgard_agg.actions USING gin (addresses);
CREATE INDEX ON midgard_agg.actions USING gin (transactions);
CREATE INDEX ON midgard_agg.actions USING gin (assets);
CREATE INDEX ON midgard_agg.actions USING gin ((meta -> 'affiliateAddress'));

--
-- Functions for actions aggregates
--

CREATE FUNCTION midgard_agg.check_synth(ta text[]) RETURNS boolean
LANGUAGE plpgsql AS $BODY$
DECLARE
    t text;
BEGIN
    FOREACH t IN ARRAY ta
    LOOP
        IF t ~ '/' THEN
            RETURN TRUE;
        END IF;
    END LOOP;
    RETURN FALSE;
END
$BODY$;

CREATE FUNCTION midgard_agg.check_no_rune(ta text[]) RETURNS boolean
LANGUAGE plpgsql AS $BODY$
DECLARE
    t text;
BEGIN
    FOREACH t IN ARRAY ta
    LOOP
        IF t='MAYA.CACAO' THEN
            RETURN FALSE;
        END IF;
    END LOOP;
    RETURN TRUE;
END
$BODY$;

CREATE FUNCTION midgard_agg.check_derived(ta text[]) RETURNS boolean
LANGUAGE plpgsql AS $BODY$
DECLARE
    t text;
BEGIN
    FOREACH t IN ARRAY ta
    LOOP
        IF t ~ 'MAYA\.(?!CACAO).+'  THEN
            RETURN TRUE;
        END IF;
    END LOOP;
    RETURN FALSE;
END
$BODY$;

CREATE FUNCTION midgard_agg.out_tx(
    txid text,
    address text,
    height text,
    internal boolean,
    VARIADIC coins coin_rec[]
) RETURNS jsonb
LANGUAGE plpgsql AS $BODY$
DECLARE
    ret jsonb;
BEGIN
    ret := jsonb_build_object('txID', txid, 'address', address, 'coins', coins(VARIADIC coins));
    IF height IS NOT NULL THEN
        ret := ret || jsonb_build_object('height', height);
    END IF;
    IF internal IS NOT NULL THEN
        ret := ret || jsonb_build_object('internal', internal);
    END IF;

    RETURN ret;
END
$BODY$;

--
-- Basic VIEWs that build actions
--

CREATE VIEW midgard_agg.switch_actions AS
    SELECT
        event_id,
        block_timestamp,
        'switch' AS action_type,
        tx :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        non_null_array(tx) AS transactions,
        ARRAY[burn_asset, 'MAYA.CACAO'] :: text[] AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (burn_asset, burn_e8))) AS ins,
        jsonb_build_array(mktransaction(NULL, to_addr, ('MAYA.CACAO', mint_e8))) AS outs,
        jsonb_build_array() AS fees,
        NULL :: jsonb AS meta
    FROM switch_events;

CREATE VIEW midgard_agg.refund_actions AS
    SELECT
        event_id,
        block_timestamp,
        'refund' AS action_type,
        tx :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        ARRAY[tx] :: text[] AS transactions,
        non_null_array(asset, asset_2nd) AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (asset, asset_e8))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object(
            'reason', reason,
            'memo', memo,
            'affiliateFee', CASE
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){4}(\d{1,5}?)(?::|$)')::int
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){3}(\d{1,5}?)(?::|$)')::int
                ELSE NULL
            END,
            'affiliateAddress', CASE
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){3}([^:]+)')
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){2}([^:]+)')
                ELSE NULL
            END
            ) AS meta
    FROM refund_events;

CREATE VIEW midgard_agg.donate_actions AS
    SELECT
        event_id,
        block_timestamp,
        'donate' AS action_type,
        tx :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        ARRAY[tx] :: text[] AS transactions,
        CASE WHEN cacao_e8 > 0 THEN ARRAY[asset, 'MAYA.CACAO']
            ELSE ARRAY[asset] END :: text[] AS assets,
        ARRAY[pool] :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (asset, asset_e8),
            ('MAYA.CACAO', cacao_e8))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        NULL :: jsonb AS meta
    FROM add_events;

CREATE VIEW midgard_agg.withdraw_actions AS
    SELECT
        event_id,
        block_timestamp,
        'withdraw' AS action_type,
        tx :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        ARRAY[tx] :: text[] AS transactions,
        CASE WHEN
            midgard_agg.check_synth(ARRAY[pool])
            THEN ARRAY[pool, 'synth']
            ELSE ARRAY[pool, 'nosynth'] END :: text[] AS assets,
        ARRAY[pool] :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (asset, asset_e8))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object(
            'asymmetry', asymmetry,
            'basisPoints', basis_points,
            'impermanentLossProtection', imp_loss_protection_e8,
            'liquidityUnits', -stake_units,
            'emitAssetE8', emit_asset_e8,
            'emitRuneE8', emit_cacao_e8,
            'memo', memo
            ) AS meta
    FROM withdraw_events;

-- calculate the latest price of the asset
CREATE VIEW midgard_agg.swap_usd_actions AS 
    SELECT
        s.*,
        COALESCE(b.cacao_e8::DOUBLE PRECISION / NULLIF(b.asset_e8::DOUBLE PRECISION, 0), 0) * r.cacao_price_e8 AS asset_price_usd,
        r.cacao_price_e8 AS cacao_price_usd
    FROM
        swap_events as s
    LEFT JOIN
        cacao_price as r ON s.block_timestamp = r.block_timestamp
    LEFT JOIN LATERAL (
        SELECT asset_e8, cacao_e8
        FROM block_pool_depths
        WHERE block_pool_depths.pool = s.pool
        AND block_pool_depths.block_timestamp <= s.block_timestamp
        ORDER BY block_timestamp DESC
        LIMIT 1
    ) AS b ON true;

-- TODO(huginn): use _direction for join
CREATE VIEW midgard_agg.swap_actions AS
    -- Double swap (same txid in different pools)
        SELECT
        swap_in.event_id,
        swap_in.block_timestamp,
        'swap' AS action_type,
        swap_in.tx :: text AS main_ref,
        ARRAY[swap_in.from_addr, swap_in.to_addr] :: text[] AS addresses,
        ARRAY[swap_in.tx] :: text[] AS transactions,
        ARRAY[swap_in.from_asset, swap_out.to_asset] :: text[] AS assets,
        CASE WHEN swap_in.pool <> swap_out.pool THEN ARRAY[swap_in.pool, swap_out.pool]
            ELSE ARRAY[swap_in.pool] END :: text[] AS pools,
        jsonb_build_array(mktransaction(swap_in.tx, swap_in.from_addr,
            (swap_in.from_asset, swap_in.from_e8))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object(
            'swapSingle', FALSE,
            'liquidityFee', swap_in.liq_fee_in_cacao_e8 + swap_out.liq_fee_in_cacao_e8,
            'swapTarget', swap_out.to_e8_min,
            'swapSlip', swap_in.swap_slip_BP + swap_out.swap_slip_BP
                - swap_in.swap_slip_BP*swap_out.swap_slip_BP/10000,
            'memo', swap_in.memo,
            'affiliateFee', CASE
                WHEN SUBSTRING(swap_in.memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(swap_in.memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){4}(\d{1,5}?)(?::|$)')::int
                WHEN SUBSTRING(swap_in.memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(swap_in.memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){3}(\d{1,5}?)(?::|$)')::int
                ELSE NULL
            END,
            'affiliateAddress', CASE
                WHEN SUBSTRING(swap_in.memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(swap_in.memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){3}([^:]+)')
                WHEN SUBSTRING(swap_in.memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(swap_in.memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){2}([^:]+)')
                ELSE NULL
            END,
            'outRuneE8', swap_in.to_e8,
            'inPriceUSD', swap_in.asset_price_usd,
            'outPriceUSD', swap_out.asset_price_usd
            ) AS meta,
        jsonb_build_object(
            'count', swap_in.streaming_count,
            'quantity', swap_in.streaming_quantity
            ) AS streaming_meta
    FROM midgard_agg.swap_usd_actions AS swap_in
    INNER JOIN midgard_agg.swap_usd_actions AS swap_out
    ON swap_in.tx = swap_out.tx AND swap_in.block_timestamp = swap_out.block_timestamp
    WHERE swap_in.from_asset <> swap_out.to_asset AND swap_in.to_e8 = swap_out.from_e8
        AND swap_in.to_asset = 'MAYA.CACAO' AND swap_out.from_asset = 'MAYA.CACAO'
        AND swap_in.memo != 'noop'
    UNION ALL
    -- Single swap (unique txid)
    SELECT
        event_id,
        block_timestamp,
        'swap' AS action_type,
        tx :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        ARRAY[tx] :: text[] AS transactions,
        ARRAY[from_asset, to_asset] :: text[] AS assets,
        ARRAY[pool] :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (from_asset, from_e8))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object(
            'swapSingle', TRUE,
            'liquidityFee', liq_fee_in_cacao_e8,
            'swapTarget', to_e8_min,
            'swapSlip', swap_slip_bp,
            'memo', memo,
            'affiliateFee', CASE
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){4}(\d{1,5}?)(?::|$)')::int
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){3}(\d{1,5}?)(?::|$)')::int
                ELSE NULL
            END,
            'affiliateAddress', CASE
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{SWAP,s,=}') THEN
                    SUBSTRING(memo FROM '^(?:=|SWAP|[s]):(?:[^:]*:){3}([^:]+)')
                WHEN SUBSTRING(memo FROM '^(.*?):')::text = ANY('{ADD,a,+}') THEN
                    SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){2}([^:]+)')
                ELSE NULL
            END,
            'inPriceUSD', CASE
                WHEN _direction%2 = 0 THEN
                    cacao_price_usd
                ELSE
                    asset_price_usd
                END,
            'outPriceUSD', CASE
                WHEN _direction%2 = 0 THEN
                    asset_price_usd
                ELSE
                    cacao_price_usd
                END
            ) AS meta,
        jsonb_build_object(
            'count', streaming_count,
            'quantity', streaming_quantity
            ) AS streaming_meta
    FROM midgard_agg.swap_usd_actions AS single_swaps
    WHERE NOT EXISTS (
        SELECT tx FROM swap_events
        WHERE block_timestamp = single_swaps.block_timestamp AND tx = single_swaps.tx
            AND (from_e8 = single_swaps.to_e8 OR to_e8 = single_swaps.from_e8)
    );

CREATE VIEW midgard_agg.addliquidity_actions AS
    SELECT
        event_id,
        block_timestamp,
        'addLiquidity' AS action_type,
        NULL :: text AS main_ref,
        non_null_array(cacao_addr, asset_addr) AS addresses,
        non_null_array(cacao_tx, asset_tx) AS transactions,
        CASE WHEN
            midgard_agg.check_synth(ARRAY[pool])
            THEN ARRAY[pool, 'synth']
            ELSE ARRAY[pool, 'MAYA.CACAO', 'nosynth'] END :: text[] AS assets,
        ARRAY[pool] :: text[] AS pools,
        transaction_list(
            mktransaction(cacao_tx, cacao_addr, ('MAYA.CACAO', cacao_e8)),
            mktransaction(asset_tx, asset_addr, (pool, asset_e8))
            ) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object(
            'status', 'success',
            'liquidityUnits', stake_units,
            'memo', memo,
            'affiliateFee', 
                SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){3}(\d{1,5}?)(?::|$)')::int,
            'affiliateAddress',
                SUBSTRING(memo FROM '^(?:ADD|[+]|a):(?:[^:]*:){2}([^:]+)')
            ) AS meta
    FROM stake_events
    UNION ALL
    -- Pending `add`s will be removed when not pending anymore
    SELECT
        event_id,
        block_timestamp,
        'addLiquidity' AS action_type,
        'PL:' || cacao_addr || ':' || pool :: text AS main_ref,
        non_null_array(cacao_addr, asset_addr) AS addresses,
        non_null_array(cacao_tx, asset_tx) AS transactions,
        ARRAY[pool, 'MAYA.CACAO'] :: text[] AS assets,
        ARRAY[pool] :: text[] AS pools,
        transaction_list(
            mktransaction(cacao_tx, cacao_addr, ('MAYA.CACAO', cacao_e8)),
            mktransaction(asset_tx, asset_addr, (pool, asset_e8))
            ) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array() AS fees,
        jsonb_build_object('status', 'pending') AS meta
    FROM pending_liquidity_events
    WHERE pending_type = 'add'
    ;

CREATE VIEW midgard_agg.send_actions AS
    SELECT
        event_id,
        block_timestamp,
        'send' AS action_type,
        tx_id :: text AS main_ref,
        ARRAY[from_addr, to_addr] :: text[] AS addresses,
        non_null_array(tx_id) AS transactions,
        ARRAY[asset] :: text[] AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx_id, from_addr, (asset, amount_e8))) AS ins,
        jsonb_build_array(mktransaction(tx_id, to_addr, (asset, amount_e8))) AS outs,
        jsonb_build_array(jsonb_build_object('asset', asset, 'amount', 20000000)) AS fees,
        jsonb_build_object('memo', memo, 'code', code, 'log', raw_log) AS meta
    FROM send_messages;

CREATE VIEW midgard_agg.mayaname_actions AS
    SELECT
        event_id,
        block_timestamp,
        'mayaname' AS action_type,
        tx_id :: text AS main_ref,
        non_null_array(address, owner, sender) AS addresses,
        non_null_array(tx_id) AS transactions,
        ARRAY['MAYA.CACAO'] :: text[] AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx_id, sender, ('MAYA.CACAO', (fund_amount_e8 + registration_fee_e8) :: bigint))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array(jsonb_build_object('asset', 'MAYA.CACAO', 'amount', 20000000)) AS fees,
        jsonb_build_object(
            'thorName', name,
            'address', address,
            'owner', owner,
            'chain', chain,
            'fundAmount', fund_amount_e8,
            'registrationFee', registration_fee_e8,
            'expire', expire,
            'memo', memo,
            'txType', 'mayaname'
            ) AS meta
    FROM mayaname_change_events;

CREATE VIEW midgard_agg.bond_actions AS
    SELECT
        event_id,
        block_timestamp,
        CASE
            WHEN SUBSTRING(LOWER(memo) FROM '^(.*?):')::text = ANY('{bond}') THEN 'bond'
            WHEN SUBSTRING(LOWER(memo) FROM '^(.*?):')::text = ANY('{unbond}') THEN 'unbond' 
            ELSE 'unknown'
        END AS action_type,
        tx :: text AS main_ref,
        non_null_array(
            from_addr, 
            to_addr, 
            CASE
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:[^:]+\.[^:]+|.{0}:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:){2}([^:]+)')
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:maya[^:]*:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:maya[^:]*:).*')
                ELSE NULL
            END) AS addresses,
        non_null_array(tx) AS transactions,
        ARRAY[asset] :: text[] AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx, from_addr, (asset, asset_e8 :: bigint))) AS ins,
        jsonb_build_array(mktransaction(tx, to_addr, (asset, e8 :: bigint))) AS outs,
        jsonb_build_array(jsonb_build_object('asset', 'THOR.RUNE', 'amount', 20000000)) AS fees,
        jsonb_build_object(
            'memo', memo,
            'nodeAddress', CASE
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:[^:]+\.[^:]+|.{0}:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:){2}([^:]+)')
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:maya[^:]*:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:){0}([^:]+)')
                ELSE NULL
            END,
            'provider', CASE
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:[^:]+\.[^:]+|.{0}:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:){3}([^:]+)')
                WHEN LOWER(memo) ~ '^(?:bond|unbond):(?:maya[^:]*:).*' THEN
                    SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:){1}([^:]+)')
                ELSE NULL
            END,
            'fee', SUBSTRING(LOWER(memo) FROM '^(?:bond|unbond):(?:[^:]*:)*(\-1|\d+){1}$')
            ) AS meta
    FROM bond_events;

CREATE VIEW midgard_agg.failed_actions AS
    SELECT
        event_id,
        block_timestamp,
        'failed' action_type,
        tx_id :: text AS main_ref,
        non_null_array(from_addr) AS addresses,
        non_null_array(tx_id) AS transactions,
        ARRAY[asset] :: text[] AS assets,
        NULL :: text[] AS pools,
        jsonb_build_array(mktransaction(tx_id, from_addr, (asset, amount_e8 :: bigint))) AS ins,
        jsonb_build_array() AS outs,
        jsonb_build_array(jsonb_build_object('asset', 'THOR.RUNE', 'amount', 20000000)) AS fees,
        jsonb_build_object(
            'memo', memo,
            'reason', reason,
            'code', code
            ) AS meta
    FROM failed_deposit_messages;

--
-- Procedures for updating actions
--

CREATE PROCEDURE midgard_agg.insert_actions(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.switch_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.refund_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.donate_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.withdraw_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.swap_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 ON CONFLICT DO NOTHING $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.addliquidity_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.send_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 ON CONFLICT DO NOTHING $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.mayaname_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 ON CONFLICT DO NOTHING $$ USING t1, t2;

    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.bond_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 ON CONFLICT DO NOTHING $$ USING t1, t2;
    
    EXECUTE $$ INSERT INTO midgard_agg.actions
    SELECT * FROM midgard_agg.failed_actions
        WHERE $1 <= block_timestamp AND block_timestamp < $2 ON CONFLICT DO NOTHING $$ USING t1, t2;
END
$BODY$;

-- TODO(muninn): Check the pending logic regarding nil rune address
CREATE PROCEDURE midgard_agg.trim_pending_actions(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    DELETE FROM midgard_agg.actions AS a
    USING stake_events AS s
    WHERE
        t1 <= s.block_timestamp AND s.block_timestamp < t2
        AND a.event_id <= s.event_id
        AND a.main_ref = 'PL:' || s.cacao_addr || ':' || s.pool;

    DELETE FROM midgard_agg.actions AS a
    USING pending_liquidity_events AS pw
    WHERE
        t1 <= pw.block_timestamp AND pw.block_timestamp < t2
        AND a.event_id <= pw.event_id
        AND pw.pending_type = 'withdraw'
        AND a.main_ref = 'PL:' || pw.cacao_addr || ':' || pw.pool;
END
$BODY$;

-- TODO(huginn): Remove duplicates from these lists?
CREATE PROCEDURE midgard_agg.actions_add_outbounds(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    UPDATE outbound_events as o
    SET
        internal = TRUE
    FROM (
        SELECT *
        FROM swap_events
        WHERE t1 <= block_timestamp AND block_timestamp < t2
        ) as a
    WHERE
        o.in_tx = a.tx AND
        o.block_timestamp = a.block_timestamp AND
        a.from_asset = 'MAYA.CACAO' AND
        o.asset_e8 = a.from_e8 AND
        o.asset = 'MAYA.CACAO'
    ;

    UPDATE midgard_agg.actions AS a
    SET
        addresses = a.addresses || o.froms || o.tos,
        transactions = a.transactions || array_remove(o.transactions, NULL),
        assets = a.assets || o.assets,
        outs = a.outs || o.outs
    FROM (
        SELECT
            in_tx,
            array_agg(from_addr :: text) AS froms,
            array_agg(to_addr :: text) AS tos,
            array_agg(tx :: text) AS transactions,
            array_agg(asset :: text) AS assets,
            jsonb_agg(midgard_agg.out_tx(tx, to_addr, TRUNC(event_id / 1e10)::text, internal, (asset, asset_e8))) AS outs
        FROM outbound_events
        WHERE t1 <= block_timestamp AND block_timestamp < t2 AND internal IS NOT TRUE
        GROUP BY in_tx
        ) AS o
    WHERE
        o.in_tx = a.main_ref;
END
$BODY$;

-- Add streaming details to swap action delete tx_id and event_id from event
CREATE PROCEDURE midgard_agg.streaming_details(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    UPDATE midgard_agg.actions AS a
    SET
        streaming_meta = a.streaming_meta || out
    FROM (
        SELECT DISTINCT ON (tx_id)
            tx_id,
            jsonb_build_object(
                'interval', interval,
                'quantity', quantity,
                'count', count,
                'last_height', last_height,
                'deposit_asset', deposit_asset,
                'deposit_e8', deposit_e8,
                'in_asset', in_asset,
                'in_e8', in_e8,
                'out_asset', out_asset,
                'out_e8', out_e8,
                'failed_swaps', failed_swaps,
                'failed_swap_reasons', failed_swap_reasons
            ) as out
        FROM streaming_swap_details_events
        WHERE t1 <= block_timestamp AND block_timestamp < t2
        ) AS s
    WHERE
        s.tx_id = a.main_ref;
END
$BODY$;

CREATE PROCEDURE midgard_agg.actions_add_fees(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    UPDATE midgard_agg.actions AS a
    SET
        fees = a.fees || f.fees
    FROM (
        SELECT
            tx,
            jsonb_agg(jsonb_build_object('asset', asset, 'amount', asset_e8)) AS fees
        FROM fee_events
        WHERE t1 <= block_timestamp AND block_timestamp < t2
        GROUP BY tx
        ) AS f
    WHERE
        f.tx = a.main_ref;
END
$BODY$;

CREATE TABLE midgard_agg.streaming_logs AS TABLE swap_events;

CREATE FUNCTION midgard_agg.add_streaming_logs() RETURNS trigger
LANGUAGE plpgsql AS $BODY$
DECLARE
    streaming_swap midgard_agg.actions;
BEGIN
    -- Look up the current state of the streaming_swap
    SELECT * FROM midgard_agg.actions
    WHERE main_ref = NEW.main_ref AND action_type = 'swap'
    FOR UPDATE INTO streaming_swap;

    -- Add new streaming swap details to old one
    IF streaming_swap.main_ref IS NOT NULL THEN

        IF NEW.ins #> '{0, "coins", 0, "asset"}' = streaming_swap.ins #> '{0, "coins", 0, "asset"}' THEN
            -- should be update specific values on the jsonb
            streaming_swap.ins := jsonb_set(streaming_swap.ins, '{0, "coins", 0, "amount"}',
                to_jsonb((streaming_swap.ins #> '{0, "coins", 0, "amount"}')::bigint + (NEW.ins #> '{0, "coins", 0, "amount"}')::bigint));
        END IF;

        -- TODO: add swap slip
        UPDATE midgard_agg.actions SET
            ins = streaming_swap.ins,
            meta = streaming_swap.meta || jsonb_build_object('SwapStreaming', true) ||
                jsonb_build_object('liquidityFee', (meta->>'liquidityFee')::bigint + (NEW.meta->>'liquidityFee')::bigint),
            streaming_meta = NEW.streaming_meta
        WHERE main_ref = streaming_swap.main_ref AND action_type = 'swap';
    END IF;


    -- Never fails, just enriches the row to be inserted and updates the `members` table.
    RETURN NEW;
END;
$BODY$;

CREATE TRIGGER add_log_trigger
    BEFORE INSERT ON midgard_agg.actions
    FOR EACH ROW
    WHEN (NEW.action_type = 'swap')
    EXECUTE FUNCTION midgard_agg.add_streaming_logs();

CREATE PROCEDURE midgard_agg.update_actions_interval(t1 bigint, t2 bigint)
LANGUAGE plpgsql AS $BODY$
BEGIN
    CALL midgard_agg.insert_actions(t1, t2);
    CALL midgard_agg.trim_pending_actions(t1, t2);
    CALL midgard_agg.actions_add_outbounds(t1, t2);
    CALL midgard_agg.streaming_details(t1,t2);
    CALL midgard_agg.actions_add_fees(t1, t2);
END
$BODY$;

INSERT INTO midgard_agg.watermarks (materialized_table, watermark)
    VALUES ('actions', 0);

CREATE PROCEDURE midgard_agg.update_actions(w_new bigint)
LANGUAGE plpgsql AS $BODY$
DECLARE
    w_old bigint;
BEGIN
    SELECT watermark FROM midgard_agg.watermarks WHERE materialized_table = 'actions'
        FOR UPDATE INTO w_old;
    IF w_new <= w_old THEN
        RAISE WARNING 'Updating actions into past: % -> %', w_old, w_new;
        RETURN;
    END IF;
    CALL midgard_agg.update_actions_interval(w_old, w_new);
    UPDATE midgard_agg.watermarks SET watermark = w_new WHERE materialized_table = 'actions';
END
$BODY$;
