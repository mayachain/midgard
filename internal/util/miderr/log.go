package miderr

import "gitlab.com/mayachain/midgard/internal/util/midlog"

func LogEventParseErrorF(format string, v ...interface{}) {
	midlog.WarnF(format, v...)
}
