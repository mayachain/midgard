package timeseries

import (
	"context"

	"gitlab.com/mayachain/midgard/internal/db"
)

type MAYANameEntry struct {
	Chain   string
	Address string
}

type MAYAName struct {
	Owner              string
	RegistrationHeight int64
	Expire             int64
	PreferredAsset     string
	AffiliateBps       int64
	Entries            []MAYANameEntry
}

type MAYANameSubAffiliate struct {
	Name         string
	ShareBps     int64
	Expire       int64
	AffiliateBps int64
}

func GetMAYAName(ctx context.Context, name string) (tName MAYAName, err error) {
	q := `
		SELECT chain, address, height, expire, preferred_asset, owner, affiliate_bps
		FROM midgard_agg.mayaname_current_state
		WHERE name = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, name)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var entry MAYANameEntry
		if err := rows.Scan(
			&entry.Chain,
			&entry.Address,
			&tName.RegistrationHeight,
			&tName.Expire,
			&tName.PreferredAsset,
			&tName.Owner,
			&tName.AffiliateBps,
		); err != nil {
			return tName, err
		}
		tName.Entries = append(tName.Entries, entry)
	}

	return
}

// GetMAYANameSubAffiliates returns all of sub-affiliates of the given MAYAName
func GetMAYANameSubAffiliates(ctx context.Context, name string) (subAffiliates []MAYANameSubAffiliate, err error) {
	q := `
		SELECT DISTINCT ON (sub_affiliates.sub_name) 
		sub_affiliates.sub_name AS sub_name, 
		sub_affiliates.affiliate_bps AS share_bps, 
		mayaname_current_state.affiliate_bps AS affiliate_bps, 
		mayaname_current_state.expire AS expire
		FROM midgard_agg.mayaname_sub_affiliates AS sub_affiliates
		JOIN midgard_agg.mayaname_current_state AS mayaname_current_state ON mayaname_current_state.name = sub_affiliates.sub_name
		WHERE sub_affiliates.name = $1 AND last_height() < expire
		ORDER BY sub_affiliates.sub_name ASC
	`

	rows, err := db.Query(ctx, q, name)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var subAffiliate MAYANameSubAffiliate
		if err := rows.Scan(&subAffiliate.Name, &subAffiliate.ShareBps, &subAffiliate.AffiliateBps, &subAffiliate.Expire); err != nil {
			return nil, err
		}
		subAffiliates = append(subAffiliates, subAffiliate)
	}

	return
}

// NOTE: there is probably a pure-postrgres means of doing this, which would be
// more performant. If we find that the performance of this query to be too
// slow, can try that. I don't imagine it being much of a problem since people
// aren't going to associate their address with 100's of mayanames
func GetMAYANamesByAddress(ctx context.Context, addr string) (names []string, err error) {
	q := `
		SELECT DISTINCT(name)
		FROM midgard_agg.mayaname_current_state
		WHERE address = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, addr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}

		names = append(names, name)
	}

	return
}

func GetMAYANamesByOwnerAddress(ctx context.Context, addr string) (names []string, err error) {
	q := `
		SELECT DISTINCT(name)
		FROM midgard_agg.mayaname_current_state
		WHERE owner = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, addr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}

		names = append(names, name)
	}

	return
}
