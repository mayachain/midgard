// Depth recorder fills keeps track of historical depth values and inserts changes
// in the block_pool_depths table.
package timeseries

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/mayachain/midgard/config"
	"gitlab.com/mayachain/midgard/internal/db"
)

// MapDiff helps to get differences between snapshots of a map.
type mapDiff struct {
	snapshot map[string]int64
}

// Save the map as the new snapshot.
func (md *mapDiff) save(newMap map[string]int64) {
	md.snapshot = map[string]int64{}
	for k, v := range newMap {
		md.snapshot[k] = v
	}
}

// Check if there is a chage for this pool.
func (md *mapDiff) diffAtKey(pool string, newMap map[string]int64) (hasDiff bool, newValue int64) {
	oldV, hasOld := md.snapshot[pool]
	newV, hasNew := newMap[pool]
	if hasNew {
		return !hasOld || oldV != newV, newV
	} else {
		return hasOld, 0
	}
}

type depthManager struct {
	assetE8DepthSnapshot mapDiff
	runeE8DepthSnapshot  mapDiff
	synthE8DepthSnapshot mapDiff
}

var depthRecorder depthManager

// Insert rows in the block_pool_depths for every changed value in the depth maps.
// If there is no change it doesn't write out anything.
// All values will be writen out together (assetDepth, runeDepth, synthDepth), even if only one of the values
// changed in the pool.
func (sm *depthManager) update(
	timestamp time.Time, assetE8DepthPerPool, runeE8DepthPerPool, synthE8DepthPerPool map[string]int64) error {
	blockTimestamp := timestamp.UnixNano()
	// We need to iterate over all 2*n maps: {old,new}{Asset,Rune,Synth}.
	// First put all pool names into a set.
	poolNames := map[string]bool{}
	accumulatePoolNames := func(m map[string]int64) {
		for pool := range m {
			poolNames[pool] = true
		}
	}
	accumulatePoolNames(assetE8DepthPerPool)
	accumulatePoolNames(runeE8DepthPerPool)
	accumulatePoolNames(synthE8DepthPerPool)
	accumulatePoolNames(sm.assetE8DepthSnapshot.snapshot)
	accumulatePoolNames(sm.runeE8DepthSnapshot.snapshot)
	accumulatePoolNames(sm.synthE8DepthSnapshot.snapshot)

	cols := []string{"pool", "asset_e8", "cacao_e8", "synth_e8", "block_timestamp"}

	var err error
	for pool := range poolNames {
		assetDiff, assetValue := sm.assetE8DepthSnapshot.diffAtKey(pool, assetE8DepthPerPool)
		runeDiff, runeValue := sm.runeE8DepthSnapshot.diffAtKey(pool, runeE8DepthPerPool)
		synthDiff, synthValue := sm.synthE8DepthSnapshot.diffAtKey(pool, synthE8DepthPerPool)
		if assetDiff || runeDiff || synthDiff {
			err = db.Inserter.Insert("block_pool_depths", cols, pool, assetValue, runeValue, synthValue, blockTimestamp)
			if err != nil {
				break
			}
		}
	}
	sm.assetE8DepthSnapshot.save(assetE8DepthPerPool)
	sm.runeE8DepthSnapshot.save(runeE8DepthPerPool)
	sm.synthE8DepthSnapshot.save(synthE8DepthPerPool)

	var maxDepth int64 = -1
	var cacaoPriceInUsd float64
	for _, pool := range config.Global.UsdPools {
		if maxDepth < assetE8DepthPerPool[pool] {
			cacaoPriceInUsd = float64(assetE8DepthPerPool[pool]) / float64(runeE8DepthPerPool[pool])
			maxDepth = assetE8DepthPerPool[pool]
		}
	}
	// If there is no depth in any of the USD pools, the price is NaN.
	if math.IsNaN(cacaoPriceInUsd) {
		cacaoPriceInUsd = 0
	}

	err = db.Inserter.Insert("cacao_price", []string{"cacao_price_e8", "block_timestamp"}, cacaoPriceInUsd, blockTimestamp)
	if err != nil {
		return err
	}

	if err != nil {
		return fmt.Errorf("error saving depths (timestamp: %d): %w", blockTimestamp, err)
	}

	return nil
}

func ResetDepthManagerForTest() {
	depthRecorder = depthManager{}
}
