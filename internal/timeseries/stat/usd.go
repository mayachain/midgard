package stat

import (
	"context"
	"fmt"
	"math"
	"net/http"

	"gitlab.com/mayachain/midgard/config"
	"gitlab.com/mayachain/midgard/internal/db"
	"gitlab.com/mayachain/midgard/internal/timeseries"
	"gitlab.com/mayachain/midgard/internal/util"
	oapigen "gitlab.com/mayachain/midgard/openapi/generated/oapigen"
)

func cacaoPriceUSDForDepths(depths timeseries.DepthMap) float64 {
	ret := math.NaN()

	cacaoPricesInUsd := []float64{}
	for _, pool := range config.Global.UsdPools {
		poolInfo, ok := depths[pool]
		if ok && poolInfo.AssetDepth > 0 && poolInfo.RuneDepth > 0 {
			cacaoPricesInUsd = append(cacaoPricesInUsd, 1/poolInfo.AssetPrice())
		}
	}

	if len(cacaoPricesInUsd) > 0 {
		ret = util.GetMedian(cacaoPricesInUsd)
	}

	return ret
}

// Returns the 1/price from the depest whitelisted pool.
func CacaoPriceUSD() float64 {
	return cacaoPriceUSDForDepths(timeseries.Latest.GetState().Pools)
}

func ServeUSDDebug(resp http.ResponseWriter, req *http.Request) {
	state := timeseries.Latest.GetState()
	for _, pool := range config.Global.UsdPools {
		poolInfo := state.PoolInfo(pool)
		if poolInfo == nil {
			fmt.Fprintf(resp, "%s - pool not found\n", pool)
		} else {
			depth := float64(poolInfo.RuneDepth) / 1e10
			cacaoPrice := poolInfo.CacaoPrice()
			fmt.Fprintf(resp, "%s - originalDepth: %d runeDepth: %.0f assetDepth: %d cacaoPriceUsd: %.2f\n", pool, poolInfo.RuneDepth, depth, poolInfo.AssetDepth, cacaoPrice)
		}
	}

	fmt.Fprintf(resp, "\n\ncacaoPriceUSD: %v", CacaoPriceUSD())
}

func GetCacaoPriceHistory(ctx context.Context, buckets db.Buckets) (oapigen.CacaoPriceHistory, error) {
	usdPrices, err := USDPriceHistory(ctx, buckets)
	if err != nil {
		return oapigen.CacaoPriceHistory{}, err
	}

	intervals := oapigen.CacaoPriceIntervals{}
	for _, usd := range usdPrices {
		interval := oapigen.CacaoPriceItem{
			StartTime:     util.IntStr(usd.Window.From.ToI()),
			EndTime:       util.IntStr(usd.Window.Until.ToI()),
			CacaoPriceUSD: util.FloatStr(usd.CacaoPriceUSD),
		}
		intervals = append(intervals, interval)
	}

	ret := oapigen.CacaoPriceHistory{
		Meta: oapigen.CacaoPriceMeta{
			StartTime:          util.IntStr(buckets.Start().ToI()),
			EndTime:            util.IntStr(buckets.End().ToI()),
			StartCacaoPriceUSD: util.FloatStr(usdPrices[0].CacaoPriceUSD),
			EndCacaoPriceUSD:   util.FloatStr(usdPrices[len(usdPrices)-1].CacaoPriceUSD),
		},
		Intervals: intervals,
	}

	return ret, nil
}
