package timeseries_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/mayachain/midgard/internal/db/testdb"
	"gitlab.com/mayachain/midgard/internal/fetch/notinchain"
	"gitlab.com/mayachain/midgard/openapi/generated/oapigen"
)

func TestNetworkAPY(t *testing.T) {
	defer testdb.StartMockThornode()()
	blocks := testdb.InitTestBlocks(t)

	// Active bond amount = 1500 rune
	testdb.RegisterThornodeNodes([]notinchain.NodeAccount{
		{Status: "Active", TotalBond: "1500"},
		{Status: "Standby", TotalBond: "123"}})

	// reserve=5200
	// blocks per year = 520 (10 weekly)
	// emission curve = 2
	// rewards per block: 5200 / (520 * 2) = 5
	testdb.RegisterThornodeReserve(5200)
	blocks.NewBlock(t, "2020-09-01 00:00:00",
		testdb.SetMimir{Key: "BlocksPerYear", Value: 520},

		testdb.AddLiquidity{Pool: "BNB.TWT-123", AssetAmount: 550, RuneAmount: 900},
		testdb.PoolActivate("BNB.TWT-123"),
	)

	//1 hour before with format of "2020-09-01 00:00:00"
	blocks.NewBlock(t, time.Now().Add(-2*time.Hour).Format("2006-01-02 15:04:05"),
		testdb.Swap{
			Pool:               "BNB.TWT-123",
			Coin:               "300 MAYA.CACAO",
			EmitAsset:          "50 BNB.BNB",
			LiquidityFeeInRune: 10,
		},
	)
	// Final depths: Rune = 1000 (900 + 100) ; Asset = 500 (550 - 50)
	// LP pooled amount is considered 2000 (double the rune amount)

	body := testdb.CallJSON(t, "http://localhost:8080/v2/network")

	var jsonApiResult oapigen.Network
	testdb.MustUnmarshal(t, body, &jsonApiResult)

	require.Equal(t, "1", jsonApiResult.ActiveNodeCount)
	require.Equal(t, "1", jsonApiResult.StandbyNodeCount)
	require.Equal(t, "1500", jsonApiResult.BondMetrics.TotalActiveBond)
	require.Equal(t, "123", jsonApiResult.BondMetrics.TotalStandbyBond)
	require.Equal(t, "5200", jsonApiResult.TotalReserve)
	///TODO: Recheck this test
	require.Equal(t, "1200", jsonApiResult.TotalPooledRune)

	require.Equal(t, "10", jsonApiResult.BlockRewards.BlockReward)

	// (Bond - Pooled) / (Bond + Pooled / IncentiveCurve)
	// (1500 - 1000) / (1500 + 500) = 500 / 2000 = 0.25
	///TODO: recheck this test
	require.Equal(t, "", jsonApiResult.PoolShareFactor)

	// Weekly income = 60 (block reward * weekly blocks + liquidity fees)
	// LP earning weekly = 15 (60 * 0.25)
	// LP weekly yield = 0.75% (weekly earning / 2*rune depth = 15 / 2*1000)
	// LP cumulative yearly yield ~ 47% ( 1.0075 ** 52)
	//TODO: recheck this test
	require.Contains(t, jsonApiResult.LiquidityAPY, "0")

	// Bonding earning = 45 (60 * 0.75)
	// Bonding weekly yield = 3% (weekly earning / active bond = 45 / 1500)
	// Bonding cumulative yearly yield ~ 365% ( 1.032 ** 52)
	//TODO: recheck this test
	require.Contains(t, jsonApiResult.BondingAPY, "38.64606446122725")
}

func TestNetworkNextChurnHeight(t *testing.T) {
	defer testdb.StartMockThornode()()
	blocks := testdb.InitTestBlocks(t)

	// ChurnInterval = 20 ; ChurnRetryInterval = 10
	blocks.NewBlock(t, "2020-09-01 00:00:00",
		testdb.SetMimir{Key: "ChurnInterval", Value: 20},
		testdb.SetMimir{Key: "ChurnRetryInterval", Value: 10},
	)

	// Last churn at block 2
	blocks.NewBlock(t, "2020-09-01 00:10:00", testdb.ActiveVault{AddVault: "addr"})

	body := testdb.CallJSON(t, "http://localhost:8080/v2/network")
	var result oapigen.Network
	testdb.MustUnmarshal(t, body, &result)

	require.Equal(t, "22", result.NextChurnHeight)

	blocks.EmptyBlocksBefore(t, 23) // Churn didn't happen at block 22

	body = testdb.CallJSON(t, "http://localhost:8080/v2/network")
	testdb.MustUnmarshal(t, body, &result)

	require.Equal(t, "32", result.NextChurnHeight)
}

func TestNetworkPoolCycle(t *testing.T) {
	defer testdb.StartMockThornode()()
	blocks := testdb.InitTestBlocks(t)

	// PoolCycle = 10
	blocks.NewBlock(t, "2020-09-01 00:00:00",
		testdb.SetMimir{Key: "PoolCycle", Value: 10},
	)

	// last block = 13
	blocks.EmptyBlocksBefore(t, 14)

	body := testdb.CallJSON(t, "http://localhost:8080/v2/network")
	var result oapigen.Network
	testdb.MustUnmarshal(t, body, &result)
	require.Equal(t, "7", result.PoolActivationCountdown)
}
