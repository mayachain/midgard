# Build Image
FROM golang:1.23.4 AS build

# ca-certificates pull in default CAs, without this https fetch from blockstore will fail
RUN apt-get update
RUN apt-get -y install make gcc ca-certificates

WORKDIR /tmp/midgard

# Cache Go dependencies like this:
COPY go.mod go.sum ./
RUN go mod download

COPY cmd cmd
COPY config config
COPY internal internal
COPY openapi openapi

# Copy the toolkit shared library.
COPY ./lib/libradix_engine_toolkit_uniffi.so /usr/lib

# Compile.
ENV CC=/usr/bin/gcc
ENV CGO_ENABLED=1
ENV CGO_LDFLAGS="-L/usr/lib -lradix_engine_toolkit_uniffi"
RUN go build -v --ldflags '-linkmode external' -installsuffix cgo ./cmd/blockstore/dump
RUN go build -v --ldflags '-linkmode external' -installsuffix cgo ./cmd/midgard
RUN go build -v --ldflags '-linkmode external' -installsuffix cgo ./cmd/trimdb
RUN go build -v --ldflags '-linkmode external' -installsuffix cgo ./cmd/statechecks
RUN go build -v --ldflags '-linkmode external' -installsuffix cgo ./cmd/nukedb

# Main Image
FROM alpine:3.14

# Install libc6-compat, libgcc and gcompat for compatibility with the uniffi shared library.
RUN apk add --no-cache libc6-compat libgcc gcompat

WORKDIR /app

RUN mkdir -p openapi/generated
COPY --from=build /usr/lib/libradix_engine_toolkit_uniffi.so /usr/lib/libradix_engine_toolkit_uniffi.so
COPY --from=build /etc/ssl/certs /etc/ssl/certs
COPY --from=build /tmp/midgard/openapi/generated/doc.html ./openapi/generated/doc.html
COPY --from=build /tmp/midgard/dump .
COPY --from=build /tmp/midgard/midgard .
COPY --from=build /tmp/midgard/statechecks .
COPY --from=build /tmp/midgard/trimdb .
COPY --from=build /tmp/midgard/nukedb .
COPY config/config.json .
COPY resources /resources

CMD [ "./midgard", "config.json" ]
